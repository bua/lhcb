/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRIMARYVERTEXCHECKER_H
#define PRIMARYVERTEXCHECKER_H 1

#include "Event/MCVertex.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "MCInterfaces/IForcedBDecayTool.h"

#include "GaudiAlg/GaudiTupleAlg.h"

typedef struct {
  LHCb::MCVertex*                pMCPV;             // pointer to MC PV
  int                            nRecTracks;        // number of reconstructed tracks from this MCPV
  int                            nRecBackTracks;    // number of reconstructed backward tracks
  int                            indexRecPVInfo;    // index to reconstructed PVInfo (-1 if not reco)
  int                            nCorrectTracks;    // correct tracks belonging to reconstructed PV
  int                            multClosestMCPV;   // multiplicity of closest reconstructable MCPV
  double                         distToClosestMCPV; // distance to closest reconstructible MCPV
  int                            decayCharm;        // type of mother particle
  int                            decayBeauty;
  std::vector<LHCb::MCParticle*> m_mcPartInMCPV;
  std::vector<LHCb::Track*>      m_recTracksInMCPV;
} MCPVInfo;

typedef struct {
public:
  int              nTracks;     // number of tracks
  int              nVeloTracks; // number of velo tracks in a vertex
  int              nLongTracks;
  double           minTrackRD; //
  double           maxTrackRD; //
  double           chi2;
  double           nDoF;
  double           d0;
  double           d0nTr;
  double           chi2nTr;
  double           mind0;
  double           maxd0;
  int              mother;
  Gaudi::XYZPoint  position;      // position
  Gaudi::XYZPoint  positionSigma; // position sigmas
  int              indexMCPVInfo; // index to MCPVInfo
  LHCb::RecVertex* pRECPV;        // pointer to REC PV
} RecPVInfo;

class PrimaryVertexChecker : public Gaudi::Functional::Consumer<
                             void( const LHCb::Tracks&, 
                                   const LHCb::RecVertices&,
                                   const LHCb::MCVertices&, 
                                   const LHCb::MCParticle::Container&),
          Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  /// Standard constructor
  PrimaryVertexChecker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  void operator()( const LHCb::Tracks& usedTracks, 
                   const LHCb::RecVertices& recoVertices,
                   const LHCb::MCVertices& mcvtx, 
                   const LHCb::MCParticle::Container& mcps) const override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  bool debugLevel() const { return msgLevel( MSG::DEBUG ) || msgLevel( MSG::VERBOSE ); }

private:
  //int         m_nTracksToBeRecble;
  //bool        m_produceHistogram;
  //bool        m_produceNtuple;
  //bool        m_requireVelo;
  //double      m_dzIsolated;
  //bool        m_matchByTracks;
  Gaudi::Property<int> m_nTracksToBeRecble{this, "nTracksToBeRecble", 5};
  Gaudi::Property<bool> m_produceHistogram{this, "produceHistogram", true};
  Gaudi::Property<bool> m_produceNtuple{this, "produceNtuple", false};
  Gaudi::Property<bool> m_requireVelo{this, "RequireVelo", true};
  Gaudi::Property<double> m_dzIsolated{this, "dzIsolated", 10.0 * Gaudi::Units::mm};
  Gaudi::Property<bool> m_matchByTracks{this, "matchByTracks", true};
  // Gaudi::Property<std::string> m_inputTracksName{this, "inputTracksName", LHCb::TrackLocation::Default};
  // Gaudi::Property<std::string> m_inputVerticesName{this, "inputVerticesName", LHCb::RecVertexLocation::Primary};

  // std::string m_inputTracksName;
  // std::string m_inputVerticesName;

  

  mutable int m_nevt;
  mutable int m_nMCPV, m_nRecMCPV;
  mutable int m_nMCPV_isol, m_nRecMCPV_isol;
  mutable int m_nMCPV_close, m_nRecMCPV_close;
  mutable int m_nFalsePV, m_nFalsePV_real;
  mutable int m_nMCPV_1mult, m_nRecMCPV_1mult;
  mutable int m_nMCPV_isol_1mult, m_nRecMCPV_isol_1mult;
  mutable int m_nMCPV_close_1mult, m_nRecMCPV_close_1mult;
  mutable int m_nRecMCPV_wrong_1mult;
  mutable int m_nMCPV_2mult, m_nRecMCPV_2mult;
  mutable int m_nMCPV_isol_2mult, m_nRecMCPV_isol_2mult;
  mutable int m_nMCPV_close_2mult, m_nRecMCPV_close_2mult;
  mutable int m_nRecMCPV_wrong_2mult;
  mutable int m_nMCPV_3mult, m_nRecMCPV_3mult;
  mutable int m_nMCPV_isol_3mult, m_nRecMCPV_isol_3mult;
  mutable int m_nMCPV_close_3mult, m_nRecMCPV_close_3mult;
  mutable int m_nRecMCPV_wrong_3mult;
  mutable int m_nL0PvOfB, m_nRecL0PvOfB;
  mutable int m_nBFalse, m_nRecBFalse;

  std::vector<MCPVInfo>::iterator closestMCPV( std::vector<MCPVInfo>& rblemcpv, std::vector<MCPVInfo>::iterator& itmc ) const;

  void collectProductss( LHCb::MCVertex* mcpv, LHCb::MCVertex* mcvtx, std::vector<LHCb::MCParticle*>& allprods ) const;
  void printRat( std::string mes, int a, int b );
  void match_mc_vertex_by_tracks( int ipv, std::vector<RecPVInfo>& rinfo, std::vector<MCPVInfo>& mcpvvec ) const;
  void match_mc_vertex_by_distance( int ipv, std::vector<RecPVInfo>& rinfo, std::vector<MCPVInfo>& mcpvvec ) const;
  void count_reconstructed_tracks( std::vector<MCPVInfo>& mcpvvec, std::vector<LHCb::Track*>& vecOfTracks,
                                   std::string trackLoc ) const;
  int  count_velo_tracks( LHCb::RecVertex* RecVtx );
  void count_reconstructible_mc_particles( std::vector<MCPVInfo>& mcpvvec ) const;
  // bool getInputTracks( std::vector<LHCb::Track*>& vecOfTracks ) const;
  // bool getInputVertices( std::vector<LHCb::RecVertex*>& vecOfVertices ) const;
  int  check_mother_particle( LHCb::RecVertex* RecVtx, std::string trackLoc ) const;
};
#endif // PRIMARYVERTEXCHECKER_H
