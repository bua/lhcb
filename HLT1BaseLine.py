###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from SetupHelper import setupComponent, setupAlgorithm, setupInputFromTestFileDB
from GaudiKernel.SystemOfUnits import mm, GeV

MCCuts = {
    "Velo" : { "01_velo" : "isVelo",
               "02_long" : "isLong",
               "03_long>5GeV" : "isLong & over5",
               "04_long_strange" :  "isLong & strange",
               "05_long_strange>5GeV" : "isLong & strange & over5",
               "06_long_fromB" : "isLong & fromB",
               "07_long_fromB>5GeV" :  "isLong & fromB & over5",
               "08_long_electrons" :  "isLong & isElectron",
               "09_long_fromB_electrons" :  "isLong & isElectron & fromB",
               "10_long_fromB_electrons_P>5GeV" :  "isLong & isElectron & over5 & fromB" },
    "VeloFull" : { "01_notElectron_Velo" : "isNotElectron & isVelo",
                   "02_notElectron_Velo_Forward" : "isNotElectron & isVelo & (MCETA>0)",
                   "03_notElectron_Velo_Backward" : "isNotElectron & isVelo & (MCETA<0)",
                   "04_notElectron_Velo_Eta25" : "isNotElectron & isVelo & (MCETA>2.0) & (MCETA<5.0)",
                   "05_notElectron_Long_Eta25" : "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
                   "06_notElectron_Long_Eta25 p>5GeV" : "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "07_notElectron_Long_Eta25 p<5GeV" : "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
                   "08_notElectron_Long_Eta25 p>3GeV pt>400MeV" : "isNotElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
                   "09_notElectron_Long_FromB_Eta25" : "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0)",
                   "10_notElectron_Long_FromB_Eta25 p>5GeV" : "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "11_notElectron_Long_FromB_Eta25 p<5GeV" : "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "12_notElectron_Long_FromB_Eta25 p>3GeV pt>400MeV" : "isNotElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
                   "13_notElectron_Long_FromD_Eta25" : "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0)",
                   "14_notElectron_Long_FromD_Eta25 p>5GeV" : "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "15_notElectron_Long_FromD_Eta25 p<5GeV" : "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "16_notElectron_Long_FromD_Eta25 p>3GeV pt>400MeV" : "isNotElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
                   "17_notElectron_Long_strange_Eta25" : "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0)",
                   "18_notElectron_Long_strange_Eta25 p>5GeV" : "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "19_notElectron_Long_strange_Eta25 p<5GeV" : "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "20_notElectron_Long_strange_Eta25 p>3GeV pt>400MeV" : "isNotElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
                   "21_Electron_Velo" : "isElectron & isVelo",
                   "22_Electron_Velo_Forward" : "isElectron & isVelo & (MCETA>0)",
                   "23_Electron_Velo_Backward" : "isElectron & isVelo & (MCETA<0)",
                   "24_Electron_Velo_Eta25" : "isElectron & isVelo & (MCETA>2.0) & (MCETA<5.0)",
                   "25_Electron_Long_Eta25" : "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0)",
                   "26_Electron_Long_Eta25 p>5GeV" : "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "27_Electron_Long_Eta25 p<5GeV" : "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "28_Electron_Long_Eta25 p>3GeV pt>400MeV" : "isElectron & isLong & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
                   "29_Electron_Long_FromB_Eta25" : "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0)",
                   "30_Electron_Long_FromB_Eta25 p>5GeV" : "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "31_Electron_Long_FromB_Eta25 p<5GeV" : "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "32_Electron_Long_FromB_Eta25 p>3GeV pt>400MeV" : "isElectron & isLong & fromB & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
                   "33_Electron_Long_FromD_Eta25" : "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0)",
                   "34_Electron_Long_FromD_Eta25 p>5GeV" : "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "35_Electron_Long_FromD_Eta25 p<5GeV" : "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "36_Electron_Long_FromD_Eta25 p>3GeV pt>400MeV" : "isElectron & isLong & fromD & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)",
                   "37_Electron_Long_strange_Eta25" : "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0)",
                   "38_Electron_Long_strange_Eta25 p>5GeV" : "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>5000)",
                   "39_Electron_Long_strange_Eta25 p<5GeV" : "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP<5000)",
                   "40_Electron_Long_strange_Eta25 p>3GeV pt>400MeV" : "isElectron & isLong & strange & (MCETA>2.0) & (MCETA<5.0) & (MCP>3000) & (MCPT>400)" },
    "Forward" : { "01_long" : "isLong",
                  "02_long>5GeV" : "isLong & over5",
                  "03_long_strange" : "isLong & strange",
                  "04_long_strange>5GeV" : "isLong & strange & over5",
                  "05_long_fromB" : "isLong & fromB",
                  "06_long_fromB>5GeV" : "isLong & fromB & over5",
                  "07_long_electrons" : "isLong & isElectron",
                  "08_long_fromB_electrons" : "isLong & isElectron & fromB",
                  "09_long_fromB_electrons_P>5GeV" : "isLong & isElectron & over5 & fromB" },
    "Up" : { "01_velo" : "isVelo",
             "02_velo+UT" : "isVelo & isUT",
             "03_velo+UT>5GeV" :  "isVelo & isUT & over5",
             "04_velo+notLong" : "isNotLong & isVelo ",
             "05_velo+UT+notLong" : "isNotLong & isVelo & isUT",
             "06_velo+UT+notLong>5GeV" : "isNotLong & isVelo & isUT & over5",
             "07_long" : "isLong",
             "08_long>5GeV" : "isLong & over5 ",
             "09_long_fromB" : "isLong & fromB",
             "10_long_fromB>5GeV" :  "isLong & fromB & over5",
             "11_long_electrons" :  "isLong & isElectron",
             "12_long_fromB_electrons" :  "isLong & isElectron & fromB",
             "13_long_fromB_electrons_P>5GeV" :  "isLong & isElectron & over5 & fromB" },
    "T" : { "01_hasT" : "isSeed ",
            "02_long" : "isLong",
            "03_long>5GeV" : "isLong & over5",
            "04_long_fromB" : "isLong & fromB",
            "05_long_fromB>5GeV" : "isLong & fromB & over5",
            "06_UT+T_strange" : "strange & isDown",
            "07_UT+T_strange>5GeV" : "strange & isDown & over5",
            "08_noVelo+UT+T_strange" : "strange & isDown & isNotVelo",
            "09_noVelo+UT+T_strange>5GeV" : "strange & isDown & over5 & isNotVelo",
            "10_UT+T_SfromDB" : "strange & isDown & ( fromB | fromD )",
            "11_UT+T_SfromDB>5GeV" : "strange & isDown & over5 & ( fromB | fromD )",
            "12_noVelo+UT+T_SfromDB>5GeV" : "strange & isDown & isNotVelo & over5 & ( fromB | fromD )" },
    "Down" : { "01_UT+T" : "isDown ",
               "02_UT+T>5GeV" : "isDown & over5",
               "03_UT+T_strange" : " strange & isDown",
               "04_UT+T_strange>5GeV" : " strange & isDown & over5",
               "05_noVelo+UT+T_strange" : " strange & isDown & isNotVelo",
               "06_noVelo+UT+T_strange>5GeV" : " strange & isDown & over5 & isNotVelo",
               "07_UT+T_fromB" : "isDown & fromB",
               "08_UT+T_fromB>5GeV" : "isDown & fromB & over5",
               "09_noVelo+UT+T_fromB" : "isDown & fromB & isNotVelo",
               "10_noVelo+UT+T_fromB>5GeV" : "isDown & fromB & over5 & isNotVelo",
               "11_UT+T_SfromDB" : " strange & isDown & ( fromB | fromD )",
               "12_UT+T_SfromDB>5GeV" : " strange & isDown & over5 & ( fromB | fromD )",
               "13_noVelo+UT+T_SfromDB" : " strange & isDown & isNotVelo & ( fromB | fromD )",
               "14_noVelo+UT+T_SfromDB>5GeV" : " strange & isDown & isNotVelo & over5 & ( fromB | fromD ) " },
    "UTForward" : { "01_long" : "isLong",
                    "02_long>5GeV" : "isLong & over5" },
    "UTDown" : { "01_has seed" : "isSeed",
                 "02_has seed +noVelo, T+UT" : "isSeed & isNotVelo & isDown",
                 "03_down+strange" : "strange & isDown",
                 "04_down+strange+>5GeV" : "strange & isDown & over5",
                 "05_pi<-Ks<-B" : "fromKsFromB",
                 "06_pi<-Ks<-B+> 5 GeV" : "fromKsFromB & over5" },
}

TriggerMCCuts = {
    "Velo" :  { "11_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
                "12_UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
    "Forward" : { "10_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
                  "11_UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
    "Up" :{ "14_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
            "15_UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
    "New" : { "long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
              "UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
    "UTForward" : { "03_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
                    "04_UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
    "UTDown" : { "07_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
                 "08_UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
    "UTNew" : { "long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger",
                "UT_long_fromB_P>3GeV_Pt>0.5GeV" : "isLong & fromB & trigger & isUT" },
}

def getMCCuts(key, triggerNumbers = False):
    cuts = MCCuts[key] if key in MCCuts else {}
    if triggerNumbers and key in TriggerMCCuts:
        cuts.update(TriggerMCCuts[key])
    return cuts

def setupHLT1Reconstruction(
        appMgr,
        hiveDataBroker,
        testFileDBkey,
        filepaths,
        GECCut=-1,
        IPCut=False,
        IPCutVal=0.1 * mm,
        VeloMinPT=0.3 * GeV,
        FTMinPT=0.4 * GeV,
        TrackBeamLinePVs=False,
        UseIOSvc = False,
        UseVeloSIMD = False,
        UseScifiTrackForwarding = False,
        UseFastForwardTracking = False,
        FTDecoVersion = 4,
        checkEfficiency = False,
        VeloModulesToSkip = [],
        FitterAlgo = 'ParameterizedKalman',
        VeloOnly=False,
        VeloPVOnly = False, 
        VeloBestPhysics = False,
        VeloFullCheck = False,
        WithMuonMatch = False,
        WithMuonID = False,
):
    '''declare all algorithm used in HLT1, returns sequence'''
    sequence = []

    evtClockSvc = setupComponent('EventClockSvc', InitialTime=1433509200000000000)
    Input = setupInputFromTestFileDB(testFileDBkey, filepaths, withEventSelector=not UseIOSvc)
    if UseIOSvc:
        setupComponent('LHCb__MDF__IOSvcMM', Input=Input)
        FetchData = setupAlgorithm('LHCb__MDF__IOAlg', appMgr, hiveDataBroker, instanceName='ReadMDFInput', RawEventLocation="/Event/DAQ/RawEvent", IOSvc="LHCb::MDF::IOSvcMM")
    else:
        FetchData = setupAlgorithm('Gaudi__Hive__FetchDataFromFile', appMgr, hiveDataBroker,
                                   instanceName='FetchDataFromFile', iovLockDependency=False, DataKeys=['/Event/DAQ/RawEvent'])

    odinPath = '/Event/DAQ/DummyODIN'
    UTTracksLocation = '/Event/Rec/Track/Upstream'
    VeloTracksLocation = '/Event/Rec/Track/Velo'
    #VertexLocation = '/Rec/Vertex/Primary'
    VertexLocation = '/Event/Rec/FutureVertex'
    SelAppendix = '_Selection'
    FilAppendix = '_Filter'
    print VertexLocation

    DummyEvtTime = setupAlgorithm('LHCb__Tests__FakeEventTimeProducer', appMgr, hiveDataBroker, instanceName='DummyEventTime',
                                  iovLockDependency=False, Start=evtClockSvc.InitialTime / 1E9, Step=0, ODIN=odinPath)

    ReserveIOV = setupAlgorithm('LHCb__DetDesc__ReserveDetDescForEvent', appMgr,
                                hiveDataBroker, instanceName='ReserveIOV', iovLockDependency=False, ODIN=odinPath)

    createODIN = setupAlgorithm('createODIN', appMgr, hiveDataBroker, iovLockDependency=False)

    ###### GEC configuration ######
    if GECCut > 0:
        GEC = setupAlgorithm('PrGECFilter', appMgr, hiveDataBroker, NumberFTUTClusters=GECCut)
        sequence.append(GEC)

    ###### VP configuration ######
    if UseVeloSIMD:
        PrPixel = setupAlgorithm('VeloClusterTrackingSIMD', appMgr, hiveDataBroker, instanceName='VeloClusterTracking',
                                 TracksLocation=VeloTracksLocation)
        
        vpConverterFoward = setupAlgorithm("TracksVPConverter", appMgr, hiveDataBroker, instanceName='ConverterVPForward',
                                           TracksLocation = PrPixel.TracksLocation, 
                                           OutputTracksLocation = PrPixel.TracksLocation+"Forward_AsTrackV2")

        vpConverterBackward = setupAlgorithm("TracksVPConverter", appMgr, hiveDataBroker, instanceName='ConverterVPBackward',
                                             TracksLocation = PrPixel.TracksBackwardLocation, 
                                             OutputTracksLocation = PrPixel.TracksLocation+"Backward_AsTrackV2")

        vpConverter = setupAlgorithm('TracksVPMergerConverter', appMgr, hiveDataBroker, instanceName='ConverterVP', 
                                     TracksForwardLocation=PrPixel.TracksLocation, 
                                     TracksBackwardLocation=PrPixel.TracksBackwardLocation, 
                                     OutputTracksLocation=PrPixel.TracksLocation+"AsTrackV2")
    else:
        if VeloBestPhysics:
            PrPixel = setupAlgorithm('PrPixelTracking', appMgr, hiveDataBroker, OutputTracksName=VeloTracksLocation,
                                     HardFlagging=False, MaxMissedOnTrack=3, MaxMissedConsecutive=1,
                                     PhiWindowsPerRegionForward = [ 5.0, 15.0, 10.0, 30.0 ],
                                     PhiWindow=5.5, PhiWindowExtrapolation=5.5,
                                     UsePhiPerRegionsForward=True, BoostPhysics=True,
                                     AlgoConfig="ForwardThenBackward")
        else:
            PrPixel = setupAlgorithm('PrPixelTracking', appMgr, hiveDataBroker, OutputTracksName=VeloTracksLocation,
                                     HardFlagging=True, SkipLoopSens=True, MaxMissedOnTrack=2, MaxMissedConsecutive=1,
                                     PhiWindow=2.5, PhiWindowExtrapolation=2.5, ModulesToSkip=VeloModulesToSkip, EarlyKill3HitTracks=True,
                                     UsePhiPerRegionsForward=False, BoostPhysics=False,
                                     AlgoConfig="OnlyForward" if VeloModulesToSkip else "ForwardThenBackward")
        VSPClus = setupAlgorithm('VSPClus', appMgr, hiveDataBroker, instanceName='VSPClustering', ModulesToSkip=VeloModulesToSkip)

        vpConverterFoward = setupAlgorithm("TracksVPConverter_Clusters", appMgr, hiveDataBroker, instanceName='ConverterVPForward',
                                           HitsLocation=VSPClus.ClusterLocation, 
                                           TracksLocation = PrPixel.OutputTracksName, OutputTracksLocation = PrPixel.OutputTracksName+"Forward_AsTrackV2")

        vpConverterBackward = setupAlgorithm("TracksVPConverter_Clusters", appMgr, hiveDataBroker, instanceName='ConverterVPBackward',
                                             HitsLocation=VSPClus.ClusterLocation,
                                             TracksLocation = PrPixel.TracksBackwardLocation, OutputTracksLocation = PrPixel.OutputTracksName+"Backward_AsTrackV2")

        print PrPixel.OutputTracksName+"Backward_AsTrackV2"
        print PrPixel.OutputTracksName+"Forward_AsTrackV2"

        vpConverter = setupAlgorithm('TracksVPMergerConverter_Clusters', appMgr, hiveDataBroker, instanceName='ConverterVP', HitsLocation=VSPClus.ClusterLocation, TracksForwardLocation=PrPixel.OutputTracksName, TracksBackwardLocation=PrPixel.TracksBackwardLocation, OutputTracksLocation=PrPixel.OutputTracksName+"AsTrackV2")

    ###### PV configuration ######
    if not TrackBeamLinePVs:
        PrPV = setupAlgorithm('PatPV3DFuture', appMgr, hiveDataBroker, instanceName='PatPV3D', InputTracks=vpConverter.OutputTracksLocation,
                                 OutputVerticesName=VertexLocation, UseBeamSpotRCut=True, BeamSpotRCut=.6, minClusterMult=4)
    else:
        #print PrPixel.OutputTracksName
        #print PrPixel.TracksBackwardLocation
        PrPV = setupAlgorithm('TrackBeamLineVertexFinderSoA', appMgr, hiveDataBroker, instanceName='TBLVertexFinder', TracksLocation=VeloTracksLocation, TracksBackwardLocation=PrPixel.TracksBackwardLocation,
                                OutputVertices=VertexLocation)

    PVFilter = setupAlgorithm('LoKi__VoidFilter', appMgr, hiveDataBroker, instanceName='FilterOnNoPVs', Code="SIZE('{}')>0".format(VertexLocation))
    sequence.append(PVFilter)

    ###### IP cut ######
    VeloUTInput = VeloTracksLocation
    VeloUTInputAsTrackV2 = vpConverter.OutputTracksLocation
    if ( IPCut ) :
        from Functors import MINIPCUT
        TrackIPFilter = setupAlgorithm('PrFilter__PrVeloTracks', appMgr, hiveDataBroker,
                                        instanceName = 'PrVeloUTIPFilter',
                                        Input  = VeloTracksLocation,
                                        Output = VeloTracksLocation+FilAppendix,
                                        Functor=MINIPCUT(IPCut=IPCutVal, Vertices=VertexLocation))
        VeloUTInput = TrackIPFilter.Output
        vpConverterWithIPCut = setupAlgorithm('TracksVPConverter_Clusters', appMgr, hiveDataBroker, instanceName='ConverterVPWithIPCut', HitsLocation=VSPClus.ClusterLocation, TracksLocation=VeloUTInput, OutputTracksLocation=VeloUTInput+"AsTrackV2")
        VeloUTInputAsTrackV2 = vpConverterWithIPCut.OutputTracksLocation

    ###### UT configuration ######
    StoreUT = setupAlgorithm('PrStoreUTHit', appMgr, hiveDataBroker, skipBanksWithErrors=True)
    PrVeloUT = setupAlgorithm('PrVeloUT', appMgr, hiveDataBroker, instanceName='PrVeloUTFast',
                               InputTracksName = VeloUTInput, OutputTracksName=UTTracksLocation, minPT=VeloMinPT)
    # it is possible to use the output of vpconverter only because forward tracks are stored before the backward ones,
    # ensuring that their index is identical to their original index in the forward only container
    utConverter = setupAlgorithm('TracksUTConverter', appMgr, hiveDataBroker, TracksVPLocation=VeloUTInputAsTrackV2, TracksUTLocation=PrVeloUT.OutputTracksName, OutputTracksLocation=PrVeloUT.OutputTracksName+"AsTrackV2")
    FTInputLocation = PrVeloUT.OutputTracksName

    ###### MuonMatch configuration ######
    if WithMuonMatch or WithMuonID:
        MuonHitMaker = setupAlgorithm('MuonRawToHits', appMgr, hiveDataBroker)

    if WithMuonMatch:
        MuonMatcher = setupAlgorithm('MuonMatchVeloUTSoA', appMgr, hiveDataBroker, instanceName='MuonMatching', InputTracks=PrVeloUT.OutputTracksName, InputMuonHits=MuonHitMaker.HitContainer, OutputTracks='Rec/Track/MuonMatchVeloUT')
        FTInputLocation = MuonMatcher.OutputTracks

    ###### FT configuration ######
    FTDec = setupAlgorithm('FTRawBankDecoder', appMgr, hiveDataBroker,
                           instanceName='createFTClusters', RawEventLocations="/Event/DAQ/RawEvent", DecodingVersion = FTDecoVersion)
    if UseScifiTrackForwarding:
        SciFiTrackForwardingConv = setupAlgorithm('SciFiTrackForwardingStoreHit', appMgr, hiveDataBroker, instanceName='SciFiTrackForwarding_Conv')
        PrForward = setupAlgorithm('SciFiTrackForwarding', appMgr, hiveDataBroker, instanceName='UpgradeForward',
                                   InputTracks=FTInputLocation, Output="Rec/Track/Forward")
        ftConverter = setupAlgorithm('TracksFTConverter', appMgr, hiveDataBroker, TracksUTLocation=utConverter.OutputTracksLocation, TracksFTLocation=PrForward.Output, OutputTracksLocation=PrForward.Output+"AsTrackV2")
    else:
        StoreFT = setupAlgorithm('PrStoreFTHit', appMgr, hiveDataBroker)
        PrForward = setupAlgorithm('PrForwardTracking', appMgr, hiveDataBroker, instanceName='PrForwardTrackingFast',
                                   InputName=FTInputLocation, MinPT=FTMinPT, Preselection=True,
                                   PreselectionPT=0.1*GeV if WithMuonMatch and IPCut else 0.3*GeV,
                                   TolYCollectX=3.5, TolYSlopeCollectX=0.001, MaxXWindow=1., MaxXGap=1.,
                                   SecondLoop=not UseFastForwardTracking, UseMomentumGuidedSearchWindow=UseFastForwardTracking)
        ftConverter = setupAlgorithm('TracksFTConverter', appMgr, hiveDataBroker, TracksUTLocation=utConverter.OutputTracksLocation, TracksFTLocation=PrForward.OutputName, OutputTracksLocation=PrForward.OutputName+"AsTrackV2")
    sequence += [PrForward]

    ###### MuonID configuration ######
    if WithMuonID:
        # convert tracks from std::vector of Track v2 to SOAExtensions framework
        # (without dedicated classes for within in SOA Containers)
        convertTracks = setupAlgorithm('MakeZipContainer__Track_v2', appMgr, hiveDataBroker,
            Input=ftConverter.OutputTracksLocation, OutputData='Rec/View/Forward',
            OutputSelection='Rec/Sel/Forward')

        # Run the MuonID on the converted tracks
        MuonID = setupAlgorithm('MuonIDHlt1Alg', appMgr, hiveDataBroker,
            InputTracks=convertTracks.OutputData, OutputMuonPID='Rec/Muon/MuonPID')

        # Zip together the tracks and muon PIDs
        MuonZipper = setupAlgorithm('MakeView__Track_v2__MuonID', appMgr, hiveDataBroker,
            InputTracks=convertTracks.OutputData, InputMuonIDs=MuonID.OutputMuonPID,
            Output='/Event/Rec/View/ForwardWithMuonID')

        # Select tracks based on both muon and track information
        from Functors import ISMUON, PT
        muonFilter = setupAlgorithm('SOAFilter__TrackWithMuonIDView', appMgr, hiveDataBroker,
            Input=MuonZipper.Output, InputSelection=convertTracks.OutputSelection,
            Output='Rec/View/ForwardWithMuonIDSelected', Functor=ISMUON & (PT > 0.75 * GeV ))

        sequence += [muonFilter]

    ###### Fit configuration ######
    if FitterAlgo:
        if FitterAlgo == 'ParameterizedKalman':
            fitter = setupAlgorithm('ParameterizedKalmanFit', appMgr, hiveDataBroker, instanceName='ForwardFitterAlgParamFast',
                                    InputName=ftConverter.OutputTracksLocation, OutputName="Rec/Track/FittedForward", MaxNumOutlier=2)
            #legacy hack
            from Configurables import SimplifiedMaterialLocator
            fitter.Extrapolator.ApplyMultScattCorr = True
            fitter.Extrapolator.ApplyEnergyLossCorr = False
            fitter.Extrapolator.ApplyElectronEnergyLossCorr = True
            fitter.Extrapolator.MaterialLocator = SimplifiedMaterialLocator()
            fitConverter = setupAlgorithm('MakeSelection__Track_v1', appMgr, hiveDataBroker, instanceName = "fitConverter", Input = fitter.OutputName, Output = fitter.OutputName+SelAppendix)
        elif FitterAlgo == 'MasterFitter':
            converter = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1TrackVector', appMgr, hiveDataBroker, instanceName='ConverterV1V2ForForwardFit', InputTracksName=ftConverter.OutputTracksLocation, OutputTracksName=ftConverter.OutputTracksLocation + 'AsTrackV1')
            locator = setupComponent('SimplifiedMaterialLocator')
            measProvider = setupComponent('MeasurementProvider')
            masterFitter = setupComponent('TrackMasterFitter', MaterialLocator=locator, MeasProvider=measProvider)
            masterFitter.Extrapolator.MaterialLocator = locator
            fitter = setupAlgorithm('VectorOfTracksFitter', appMgr, hiveDataBroker, instanceName='ForwardFitterAlgFast', Fitter=masterFitter, TracksInContainer=converter.OutputTracksName, TracksOutContainer='Rec/Track/FittedForward')
        elif FitterAlgo == 'VectorFitter':
            converter = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1TrackVector', appMgr, hiveDataBroker, instanceName='ConverterV1V2ForForwardFit', InputTracksName=ftConverter.OutputTracksLocation, OutputTracksName=ftConverter.OutputTracksLocation + 'AsTrackV1')
            locator = setupComponent('SimplifiedMaterialLocator')
            vectorFitter = setupComponent('TrackVectorFitter', MaterialLocator=locator, MaxNumberOutliers = 0, NumberFitIterations = 2, AddDefaultReferenceNodes = False)
            vectorFitter.Extrapolator.MaterialLocator = locator
            fitter = setupAlgorithm('VectorOfTracksFitter', appMgr, hiveDataBroker, instanceName='ForwardFitterAlgVectorFast', Fitter=vectorFitter, TracksInContainer=converter.OutputTracksName, TracksOutContainer='Rec/Track/FittedForward')
        elif FitterAlgo == 'VeloKalman':
            fitter = setupAlgorithm('VeloKalman', appMgr, hiveDataBroker, HitsLocation=PrPixel.HitsLocation, TracksVPLocation=VeloUTInput, TracksFTLocation=ftConverter.TracksFTLocation, OutputTracksLocation='Rec/Track/FittedForward')
        else:
            raise Exception('Unknown fitter algo ' + FitterAlgo)
        sequence += [fitter]

    if VeloOnly or VeloPVOnly:
        sequence = [GEC,  PrPixel]

    if VeloPVOnly: 
        sequence += [vpConverter, vpConverterFoward, vpConverterBackward, PrPV]

    ###### checkers configuration ######
    if checkEfficiency:
        FetchData.DataKeys += ["/Event/Link/Raw/VP/Digits","/Event/Link/Raw/UT/Clusters", "/Event/Link/Raw/FT/LiteClusters",
                               "/Event/pSim/MCParticles","/Event/pSim/MCVertices", "/Event/MC/TrackInfo"]
        mc_sequence = setupMCMatching(appMgr, hiveDataBroker,
                                      vpConverter.OutputTracksLocation.toStringProperty(),
                                      utConverter.OutputTracksLocation.toStringProperty(),
                                      ftConverter.OutputTracksLocation.toStringProperty(), "PrCheckerPlots.root",
                                      VeloOnly=VeloOnly, VeloPVOnly=VeloPVOnly, VeloFullCheck=VeloFullCheck)
        sequence += mc_sequence

    return sequence

def makeLinkPath(TrackLocation_v1):
    if TrackLocation_v1[0:7]=="/Event/":
        return TrackLocation_v1[0:7]+"Link/"+TrackLocation_v1[7:]
    return "Link/"+TrackLocation_v1

def setupMCMatching(appMgr, hiveDataBroker, VPLocation, UTLocation, FTLocation, OutputFile=None, VeloOnly=False, VeloPVOnly =False, VeloFullCheck=False):
    VPClusFull = setupAlgorithm('VPClusFull', appMgr, hiveDataBroker, instanceName = 'VPClusFull')
    VPFull2MC = setupAlgorithm('VPFullCluster2MCParticleLinker', appMgr, hiveDataBroker, instanceName = 'VPFullCluster2MCParticleLinker')
    linksToPRHits = '/Event/Link/Pr/LHCbID'
    ID2MC = setupAlgorithm('PrLHCbID2MCParticle', appMgr, hiveDataBroker, instanceName = "PrLHCbID2MCParticle",
                           MCParticlesLocation = 'MC/Particles',
                           VPFullClustersLocation = 'Raw/VP/FullClusters',
                           VPFullClustersLinkLocation = 'Link/Raw/VP/FullClusters',
                           UTHitsLocation = 'UT/UTHits',
                           UTHitsLinkLocation = 'Link/Raw/UT/Clusters',
                           FTLiteClustersLocation = 'Raw/FT/LiteClusters',
                           FTLiteClustersLinkLocation = 'Link/Raw/FT/LiteClusters',
                           TargetName = linksToPRHits)
    MCParticles = setupAlgorithm('UnpackMCParticle', appMgr, hiveDataBroker)
    MCVertices = setupAlgorithm('UnpackMCVertex', appMgr, hiveDataBroker)

    # The number determines if more or less histograms are generated
    writeHists = 2 if OutputFile != None else 0

    # to fix the LokiGendecorator problem
    from Configurables import LoKi__Hybrid__MCTool
    myFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    myFactory.Modules = [ "LoKiMC.decorators" ]

    # setup histogram persistency
    if OutputFile != None:
        setupComponent('HistogramPersistencySvc', OutputFile=OutputFile)
        appMgr.HistogramPersistency = "ROOT"

    ###### Velo checker ######
    VPLocation_v1 = VPLocation + "_v1"
    print VPLocation_v1
    VeloTrackConverter = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1Track', appMgr, hiveDataBroker,
                                        instanceName = "TrackConverterVelo", InputTracksName = VPLocation, OutputTracksName = VPLocation_v1)
    veloLinksToTracks = makeLinkPath(VPLocation_v1)
    VeloTrackAssociator = setupAlgorithm('PrTrackAssociator', appMgr, hiveDataBroker, instanceName = "VeloTrackAssociator",
                                       SingleContainer = VPLocation_v1, LinkerLocationID = linksToPRHits, OutputLocation = veloLinksToTracks)
    VPTrackChecker = setupAlgorithm('PrTrackChecker', appMgr, hiveDataBroker, Tracks=VPLocation_v1, Links=veloLinksToTracks,
                                    instanceName="PrCheckerVelo", Title="Velo", HitTypesToCheck=3, WriteHistos=writeHists, MyCuts=getMCCuts("Velo"))

    sequence = [VPTrackChecker]

    if VeloFullCheck:
        VPTrackChecker2 = setupAlgorithm('PrTrackChecker', appMgr, hiveDataBroker, Tracks=VPLocation_v1, Links=veloLinksToTracks,
                                         instanceName="PrCheckerFullVelo", Title="Velo", HitTypesToCheck=3, WriteHistos=writeHists, MyCuts=getMCCuts("VeloFull"))
        sequence += [VPTrackChecker2]

    if VeloOnly:
        return sequence

    
    VeloTrackConverterBackward = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1Track', appMgr, hiveDataBroker,
                                        instanceName = "TrackConverterVeloBackward", InputTracksName = "/Event/Rec/Track/VeloBackward_AsTrackV2", OutputTracksName = "/Event/Rec/Track/VeloBackward_AsTrackV1")

    VeloTrackConverterForward = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1Track', appMgr, hiveDataBroker,
                                        instanceName = "TrackConverterVeloForward", InputTracksName = "/Event/Rec/Track/VeloForward_AsTrackV2", OutputTracksName = "/Event/Rec/Track/VeloForward_AsTrackV1")


    PVConverter = setupAlgorithm('LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex', appMgr, hiveDataBroker,
                                 instanceName = "VertexConverter", 
                                 InputBackwardTracks = "/Event/Rec/Track/VeloBackward_AsTrackV1",  InputForwardTracks = "/Event/Rec/Track/VeloForward_AsTrackV1",
                                 InputBackwardTracksv2 = "/Event/Rec/Track/VeloBackward_AsTrackV2", InputForwardTracksv2 = "/Event/Rec/Track/VeloForward_AsTrackV2",
                                 InputVerticesName ='/Event/Rec/FutureVertex', OutputVerticesName = '/Event/Rec/Vertex/Primary')

    PVChecker = setupAlgorithm('PrimaryVertexChecker', appMgr, hiveDataBroker, instanceName = "PrimaryVertexChecker", 
                               nTracksToBeRecble = 4, produceNtuple = True, matchByTracks = False, inputVerticesName = '/Event/Rec/Vertex/Primary', 
                               inputTracksName = VPLocation_v1)
    sequence +=  [VeloTrackConverterBackward, VeloTrackConverterForward, PVConverter, PVChecker]      
    if VeloPVOnly: 
        return sequence 
    



    ###### UT checker ######
    UTLocation_v1 = UTLocation + "_v1"
    UTTrackConverter = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1Track', appMgr, hiveDataBroker,
                                    instanceName = "TrackConverterUT", InputTracksName = UTLocation, OutputTracksName = UTLocation_v1)
    utLinksToTracks = makeLinkPath(UTLocation_v1)
    UTTrackAssociator = setupAlgorithm('PrTrackAssociator', appMgr, hiveDataBroker, instanceName = "UTTrackAssociator",
                                       SingleContainer = UTLocation_v1, LinkerLocationID = linksToPRHits, OutputLocation = utLinksToTracks)
    UTTrackChecker = setupAlgorithm('PrTrackChecker', appMgr, hiveDataBroker, Tracks=UTLocation_v1, Links=utLinksToTracks,
                          instanceName="PrCheckerUT", Title="UT", HitTypesToCheck=4, WriteHistos=writeHists, MyCuts=getMCCuts("Up"))

    ###### FT checker ######
    FTLocation_v1 = FTLocation + "_v1"
    FTTrackConverter = setupAlgorithm('LHCb__Converters__Track__v1__fromV2TrackV1Track', appMgr, hiveDataBroker,
                                    instanceName = "TrackConverterFT", InputTracksName = FTLocation, OutputTracksName = FTLocation_v1)
    ftLinksToTracks = makeLinkPath(FTLocation_v1)
    FTTrackAssociator = setupAlgorithm('PrTrackAssociator', appMgr, hiveDataBroker, instanceName = "FTTrackAssociator",
                                       SingleContainer = FTLocation_v1, LinkerLocationID = linksToPRHits,
                                       OutputLocation = ftLinksToTracks)

    FTTrackChecker = setupAlgorithm('PrTrackChecker', appMgr, hiveDataBroker, Tracks=FTLocation_v1, Links="Link/"+FTLocation_v1, instanceName="PrCheckerForward", Title="Forward", HitTypesToCheck=8, WriteHistos=writeHists, MyCuts=getMCCuts("Forward"))
    UTHitChecker = setupAlgorithm('PrUTHitChecker', appMgr, hiveDataBroker, Tracks=FTLocation_v1, Links="Link/"+FTLocation_v1, instanceName="PrCheckerUTForward", Title="UTForward", WriteHistos=writeHists, MyCuts=getMCCuts("UTForward"))
    FTTrackChecker.addTool( myFactory )
    UTHitChecker.addTool( myFactory )

    sequence += [UTTrackChecker, FTTrackChecker, UTHitChecker]

    return sequence
