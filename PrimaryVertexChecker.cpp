/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "AIDA/IHistogram1D.h"
#include "DetDesc/Condition.h"
#include "Event/L0DUReport.h"
#include "Event/RecVertex.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiUtils/HistoStats.h"
#include "Linker/AllLinks.h"
#include "Linker/LinkerWithKey.h"
#include "VeloDet/DeVelo.h"
#include <Event/MCTrackInfo.h>
#include <Linker/LinkedTo.h>
#include "GaudiAlg/Consumer.h"
// local
#include "PrimaryVertexChecker.h"

bool sortmlt( MCPVInfo first, MCPVInfo second ) { return first.nRecTracks > second.nRecTracks; }

//-----------------------------------------------------------------------------
// Implementation file for class : PrimaryVertexChecker
//-----------------------------------------------------------------------------



// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrimaryVertexChecker ) 

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrimaryVertexChecker::PrimaryVertexChecker( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                                      {
                                          KeyValue{"inputTracksName", LHCb::TrackLocation::Default},
                                          KeyValue{"inputVerticesName", LHCb::RecVertexLocation::Primary},
                                          KeyValue{"MCVerticesInput", LHCb::MCVertexLocation::Default},
                                          KeyValue{"MCParticlesInput", LHCb::MCParticleLocation::Default},
                                      } ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrimaryVertexChecker::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // Must be executed first
  if ( sc.isFailure() ) debug() << "==> Initialize" << endmsg;

  m_nevt                 = 0;
  m_nMCPV                = 0;
  m_nRecMCPV             = 0;
  m_nMCPV_isol           = 0;
  m_nRecMCPV_isol        = 0;
  m_nMCPV_close          = 0;
  m_nRecMCPV_close       = 0;
  m_nFalsePV             = 0;
  m_nFalsePV_real        = 0;
  m_nMCPV_1mult          = 0;
  m_nRecMCPV_1mult       = 0;
  m_nMCPV_isol_1mult     = 0;
  m_nRecMCPV_isol_1mult  = 0;
  m_nMCPV_close_1mult    = 0;
  m_nRecMCPV_close_1mult = 0;
  m_nMCPV_2mult          = 0;
  m_nRecMCPV_2mult       = 0;
  m_nMCPV_isol_2mult     = 0;
  m_nRecMCPV_isol_2mult  = 0;
  m_nMCPV_close_2mult    = 0;
  m_nRecMCPV_close_2mult = 0;
  m_nMCPV_3mult          = 0;
  m_nRecMCPV_3mult       = 0;
  m_nMCPV_isol_3mult     = 0;
  m_nRecMCPV_isol_3mult  = 0;
  m_nMCPV_close_3mult    = 0;
  m_nRecMCPV_close_3mult = 0;
  // for L0 accepted PV of B signal
  m_nL0PvOfB    = 0;
  m_nRecL0PvOfB = 0;
  m_nBFalse     = 0;
  m_nRecBFalse  = 0;

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
void PrimaryVertexChecker::operator()( const LHCb::Tracks& usedTracks, 
                   const LHCb::RecVertices& recoVertices,
                   const LHCb::MCVertices& mcvtx, 
                   const LHCb::MCParticle::Container& mcps) const {

  debug() << "==> Execute" << endmsg;

  // Event
  m_nevt++;

  // Get input
  std::vector<LHCb::Track*>     vecOfTracks;
  std::vector<LHCb::RecVertex*> vecOfVertices;
  std::string                   trackLoc = "/Event/Rec/Track/VeloAsTrackV2_v1";
  bool                          tracks_ok   = true; //getInputTracks( vecOfTracks );

  if ( usedTracks.size() == 0 ) tracks_ok = false;

  std::vector<LHCb::Track*>::const_iterator itT;
  for ( itT = usedTracks.begin(); usedTracks.end() != itT; itT++ ) {
    LHCb::Track* ptr = ( *itT );
    vecOfTracks.push_back( ptr );
  }

  if ( !tracks_ok && m_matchByTracks ) {
    return; // return SUCCESSS anyway
  }

 // bool                          vertices_ok = getInputVertices( vecOfVertices );
  std::vector<LHCb::RecVertex*>::const_iterator itVer;
  for ( itVer = recoVertices.begin(); recoVertices.end() != itVer; itVer++ ) {
    LHCb::RecVertex* ppv = ( *itVer );
    vecOfVertices.push_back( ppv );
  }

//  if ( !vertices_ok ) { return; }

  std::string m_beamSpotCond = "/dd/Conditions/Online/Velo/MotionSystem";
  //  Condition *myCond =  get<Condition>(detSvc(), m_beamSpotCond );
  const double xRC = 0.0; // myCond -> paramAsDouble ( "ResolPosRC" ) ;
  const double xLA = 0.0; // myCond -> paramAsDouble ( "ResolPosLA" ) ;
  const double Y   = 0.0; // myCond -> paramAsDouble ( "ResolPosY"  ) ;

  double m_beamSpotX = ( xRC + xLA ) / 2;
  double m_beamSpotY = Y;

  if ( debugLevel() )
    debug() << trackLoc << " # tracks: " << vecOfTracks.size() << "  # vertices: " << vecOfVertices.size() << endmsg;

  // Fill reconstucted PV info
  std::vector<RecPVInfo>                  recpvvec;
  std::vector<LHCb::RecVertex*>::iterator itRecV;
  for ( itRecV = vecOfVertices.begin(); vecOfVertices.end() != itRecV; itRecV++ ) {
    LHCb::RecVertex* pv;
    pv = *itRecV;
    RecPVInfo recinfo;
    recinfo.pRECPV            = pv;
    recinfo.position          = pv->position();
    Gaudi::SymMatrix3x3 covPV = pv->covMatrix();
    double              sigx  = sqrt( covPV( 0, 0 ) );
    double              sigy  = sqrt( covPV( 1, 1 ) );
    double              sigz  = sqrt( covPV( 2, 2 ) );
    Gaudi::XYZPoint     a3d( sigx, sigy, sigz );
    recinfo.positionSigma = a3d;
    recinfo.nTracks       = pv->tracks().size();
    double minRD          = 99999.;
    double maxRD          = -99999.;
    double chi2           = pv->chi2();
    double nDoF           = pv->nDoF();

    std::string                               container = "container";
    typedef const SmartRefVector<LHCb::Track> PVTRACKS;
    PVTRACKS&                                 tracksIn = pv->tracks();
    PVTRACKS::const_iterator                  itin;

    int    mother    = 0;
    int    velo      = 0;
    int    lg        = 0;
    double d0        = 0;
    double mind0     = 99999.0;
    double maxd0     = -99999.0;
    double trackChi2 = 0.0;
    int    tr        = 0;

    for ( itin = tracksIn.begin(); itin != tracksIn.end(); itin++ ) {
      tr++;
      const LHCb::Track* ptr = *itin;

      if ( ptr->type() == LHCb::Track::Types::Long ) { lg++; }
      if ( ptr->type() == LHCb::Track::Types::Velo ) { velo++; }

      double           x        = ptr->position().x();
      double           y        = ptr->position().y();
      double           r2       = x * x + y * y;
      Gaudi::XYZVector udir     = ptr->firstState().slopes().Unit();
      Gaudi::XYZVector distance = ptr->firstState().position() - pv->position();
      Gaudi::XYZVector d0i      = udir.Cross( distance.Cross( udir ) );

      d0 = d0 + d0i.Mag2();
      if ( d0i.Mag2() > maxd0 ) { maxd0 = d0i.Mag2(); }
      if ( d0i.Mag2() < mind0 ) { mind0 = d0i.Mag2(); }

      if ( r2 > maxRD ) { maxRD = r2; }
      if ( r2 < minRD ) { minRD = r2; }

      double trChi2 = ptr->chi2();
      trackChi2     = trackChi2 + trChi2;
    }

    if ( m_matchByTracks ) { mother = check_mother_particle( pv, trackLoc ); }

    recinfo.minTrackRD    = minRD;
    recinfo.maxTrackRD    = maxRD;
    recinfo.mother        = mother;
    recinfo.chi2          = chi2;
    recinfo.nDoF          = nDoF;
    recinfo.d0            = d0;
    recinfo.d0nTr         = (double)d0 / (double)tr;
    recinfo.chi2nTr       = (double)trackChi2 / (double)tr;
    recinfo.mind0         = mind0;
    recinfo.maxd0         = maxd0;
    recinfo.nVeloTracks   = velo;
    recinfo.nLongTracks   = lg;
    recinfo.indexMCPVInfo = -1;
    recpvvec.push_back( recinfo );
  }

  // Fill MC PV info
  std::vector<MCPVInfo> mcpvvec;
  //LHCb::MCVertices*     mcvtx = get<LHCb::MCVertices>( LHCb::MCVertexLocation::Default );
  for ( LHCb::MCVertices::const_iterator itMCV = mcvtx.begin(); mcvtx.end() != itMCV; itMCV++ ) {
    const LHCb::MCParticle* motherPart = ( *itMCV )->mother();
    if ( 0 == motherPart ) {
      if ( ( *itMCV )->type() == LHCb::MCVertex::MCVertexType::ppCollision ) {
        MCPVInfo mcprimvert;
        mcprimvert.pMCPV             = *itMCV;
        mcprimvert.nRecTracks        = 0;
        mcprimvert.nRecBackTracks    = 0;
        mcprimvert.indexRecPVInfo    = -1;
        mcprimvert.nCorrectTracks    = 0;
        mcprimvert.multClosestMCPV   = 0;
        mcprimvert.distToClosestMCPV = 999999.;
        mcprimvert.decayBeauty       = 0;
        mcprimvert.decayCharm        = 0;
        mcprimvert.m_mcPartInMCPV.clear();
        mcprimvert.m_recTracksInMCPV.clear();
        mcpvvec.push_back( mcprimvert );
      }
    }
  }

  //const LHCb::MCParticle::Container* mcps = get<LHCb::MCParticle::Container>( LHCb::MCParticleLocation::Default );

  int sMCP = 0;
  int sTr  = vecOfTracks.size();
  for ( LHCb::MCParticle::Container::const_iterator imc = mcps.begin(); mcps.end() != imc; ++imc ) {
    const LHCb::MCParticle* dec = *imc;
    double                  z   = dec->originVertex()->position().z();
    if ( dec->particleID().threeCharge() != 0 && ( z < 400.0 && z > -600.0 ) ) { sMCP++; }
  }

  if ( m_matchByTracks ) {

    count_reconstructed_tracks( mcpvvec, vecOfTracks, trackLoc );

  } else {

    count_reconstructible_mc_particles( mcpvvec );
  }

  std::sort( mcpvvec.begin(), mcpvvec.end(), sortmlt );

  std::vector<MCPVInfo> rblemcpv;
  std::vector<MCPVInfo> not_rble_but_visible;
  std::vector<MCPVInfo> not_rble;
  int                   nmrc = 0;

  std::vector<MCPVInfo>::iterator itmc;
  for ( itmc = mcpvvec.begin(); mcpvvec.end() != itmc; itmc++ ) {
    rblemcpv.push_back( *itmc );
    if ( itmc->nRecTracks < m_nTracksToBeRecble ) { nmrc++; }
    if ( itmc->nRecTracks < m_nTracksToBeRecble && itmc->nRecTracks > 1 ) { not_rble_but_visible.push_back( *itmc ); }
    if ( itmc->nRecTracks < m_nTracksToBeRecble && itmc->nRecTracks < 2 ) { not_rble.push_back( *itmc ); }
  }

  // match MC and REC PVs
  if ( m_matchByTracks ) {

    for ( int ipv = 0; ipv < (int)recpvvec.size(); ipv++ ) { match_mc_vertex_by_tracks( ipv, recpvvec, rblemcpv ); }

  } else {

    for ( int ipv = 0; ipv < (int)recpvvec.size(); ipv++ ) { match_mc_vertex_by_distance( ipv, recpvvec, rblemcpv ); }
  }

  if ( debugLevel() ) {
    debug() << endmsg << " MC vertices " << endmsg;
    debug() << " ===================================" << endmsg;
    for ( int imcpv = 0; imcpv < (int)rblemcpv.size(); imcpv++ ) {
      std::string     ff   = " ";
      LHCb::MCVertex* mcpv = rblemcpv[imcpv].pMCPV;
      if ( rblemcpv[imcpv].indexRecPVInfo < 0 ) ff = "  NOTRECO";
      debug() << format( " %3d %3d  xyz ( %7.4f %7.4f %8.3f )   nrec = %4d", imcpv, rblemcpv[imcpv].indexRecPVInfo,
                         mcpv->position().x(), mcpv->position().y(), mcpv->position().z(), rblemcpv[imcpv].nRecTracks )
              << ff << endmsg;
    }
    debug() << " -----------------------------------" << endmsg << endmsg;

    debug() << endmsg << " REC vertices " << endmsg;
    debug() << " ===================================" << endmsg;
    for ( int ipv = 0; ipv < (int)recpvvec.size(); ipv++ ) {
      std::string ff = " ";
      if ( recpvvec[ipv].indexMCPVInfo < 0 ) ff = "  FALSE  ";
      debug()
          << format(
                 " %3d %3d  xyz ( %7.4f %7.4f %8.3f )  ntra = %4d   sigxyz ( %7.4f %7.4f %8.4f )   chi2/NDF = %7.2f",
                 ipv, recpvvec[ipv].indexMCPVInfo, recpvvec[ipv].position.x(), recpvvec[ipv].position.y(),
                 recpvvec[ipv].position.z(), recpvvec[ipv].nTracks, recpvvec[ipv].nVeloTracks,
                 recpvvec[ipv].positionSigma.x(), recpvvec[ipv].positionSigma.y(), recpvvec[ipv].positionSigma.z(),
                 recpvvec[ipv].pRECPV->chi2PerDoF() )
          << ff << endmsg;
    }
    debug() << " -----------------------------------" << endmsg;
  }

  // find nr of false PV

  int nFalsePV      = 0;
  int nFalsePV_real = 0;
  for ( int ipv = 0; ipv < (int)recpvvec.size(); ipv++ ) {
    int    fake       = 0;
    double x          = recpvvec[ipv].position.x();
    double y          = recpvvec[ipv].position.y();
    double z          = recpvvec[ipv].position.z();
    double r          = std::sqrt( x * x + y * y );
    double errx       = recpvvec[ipv].positionSigma.x();
    double erry       = recpvvec[ipv].positionSigma.y();
    double errz       = recpvvec[ipv].positionSigma.z();
    double errr       = std::sqrt( ( ( x * errx ) * ( x * errx ) + ( y * erry ) * ( y * erry ) ) / ( x * x + y * y ) );
    double minRDTrack = recpvvec[ipv].minTrackRD;
    double maxRDTrack = recpvvec[ipv].maxTrackRD;
    int    mother     = recpvvec[ipv].mother;
    double velo       = recpvvec[ipv].nVeloTracks;
    double lg         = recpvvec[ipv].nLongTracks;
    double d0         = recpvvec[ipv].d0;
    double d0nTr      = recpvvec[ipv].d0nTr;
    double chi2nTr    = recpvvec[ipv].chi2nTr;
    double mind0      = recpvvec[ipv].mind0;
    double maxd0      = recpvvec[ipv].maxd0;
    double chi2       = recpvvec[ipv].chi2;
    double nDoF       = recpvvec[ipv].nDoF;

    if ( recpvvec[ipv].indexMCPVInfo < 0 ) {
      nFalsePV++;
      fake           = 1;
      bool vis_found = false;
      for ( unsigned int imc = 0; imc < not_rble_but_visible.size(); imc++ ) {
        if ( not_rble_but_visible[imc].indexRecPVInfo > -1 ) continue;
        double dist = fabs( mcpvvec[imc].pMCPV->position().z() - recpvvec[ipv].position.z() );
        if ( dist < 5.0 * recpvvec[ipv].positionSigma.z() ) {
          vis_found                                = true;
          not_rble_but_visible[imc].indexRecPVInfo = 10;
          break;
        }
      } // imc
      if ( !vis_found ) nFalsePV_real++;
    }
    if ( m_produceNtuple ) {
      Tuple myTuple2 = nTuple( 102, "PV_nTuple2", CLID_ColumnWiseTuple );
      myTuple2->column( "fake", double( fake ) );
      myTuple2->column( "r", double( r ) );
      myTuple2->column( "x", double( x ) );
      myTuple2->column( "y", double( y ) );
      myTuple2->column( "z", double( z ) );
      myTuple2->column( "errr", double( errr ) );
      myTuple2->column( "errz", double( errz ) );
      myTuple2->column( "errx", double( errx ) );
      myTuple2->column( "erry", double( erry ) );
      myTuple2->column( "minRDTrack", double( minRDTrack ) );
      myTuple2->column( "maxRDTrack", double( maxRDTrack ) );
      myTuple2->column( "mother", double( mother ) );
      myTuple2->column( "velo", double( velo ) );
      myTuple2->column( "long", double( lg ) );
      myTuple2->column( "d0", double( d0 ) );
      myTuple2->column( "d0nTr", double( d0nTr ) );
      myTuple2->column( "chi2nTr", double( chi2nTr ) );
      myTuple2->column( "mind0", double( mind0 ) );
      myTuple2->column( "maxd0", double( maxd0 ) );
      myTuple2->column( "chi2", double( chi2 ) );
      myTuple2->column( "nDoF", double( nDoF ) );
      myTuple2->write();
    }
  }
  // Fill distance to closest recble MC PV and its multiplicity
  std::vector<MCPVInfo>::iterator itmcl;
  for ( itmcl = rblemcpv.begin(); rblemcpv.end() != itmcl; itmcl++ ) {
    std::vector<MCPVInfo>::iterator cmc  = closestMCPV( rblemcpv, itmcl );
    double                          dist = 999999.;
    int                             mult = 0;
    if ( cmc != rblemcpv.end() ) {
      dist = ( cmc->pMCPV->position() - itmcl->pMCPV->position() ).R();
      mult = cmc->nRecTracks;
    }
    itmcl->distToClosestMCPV = dist;
    itmcl->multClosestMCPV   = mult;
  }

  // count non-reconstructible close and isolated PVs
  int nmrc_isol  = 0;
  int nmrc_close = 0;

  // Counters
  int nMCPV                = rblemcpv.size() - nmrc;
  int nRecMCPV             = 0;
  int nMCPV_isol           = 0;
  int nRecMCPV_isol        = 0;
  int nMCPV_close          = 0;
  int nRecMCPV_close       = 0;
  int nMCPV_1mult          = 0;
  int nRecMCPV_1mult       = 0;
  int nMCPV_isol_1mult     = 0;
  int nRecMCPV_isol_1mult  = 0;
  int nMCPV_close_1mult    = 0;
  int nRecMCPV_close_1mult = 0;
  int nRecMCPV_wrong_1mult = 0;
  int nMCPV_2mult          = 0;
  int nRecMCPV_2mult       = 0;
  int nMCPV_isol_2mult     = 0;
  int nRecMCPV_isol_2mult  = 0;
  int nMCPV_close_2mult    = 0;
  int nRecMCPV_close_2mult = 0;
  int nRecMCPV_wrong_2mult = 0;
  int nMCPV_3mult          = 0;
  int nRecMCPV_3mult       = 0;
  int nMCPV_isol_3mult     = 0;
  int nRecMCPV_isol_3mult  = 0;
  int nMCPV_close_3mult    = 0;
  int nRecMCPV_close_3mult = 0;
  int nRecMCPV_wrong_3mult = 0;

  for ( itmc = rblemcpv.begin(); rblemcpv.end() != itmc; itmc++ ) {
    if ( itmc->distToClosestMCPV > m_dzIsolated ) nMCPV_isol++;
    if ( itmc->distToClosestMCPV > m_dzIsolated && itmc->nRecTracks < m_nTracksToBeRecble ) nmrc_isol++;
    if ( itmc->distToClosestMCPV < m_dzIsolated ) nMCPV_close++;
    if ( itmc->distToClosestMCPV < m_dzIsolated && itmc->nRecTracks < m_nTracksToBeRecble ) nmrc_close++;

    if ( itmc->indexRecPVInfo > -1 ) {
      nRecMCPV++;
      if ( itmc->distToClosestMCPV > m_dzIsolated ) nRecMCPV_isol++;
      if ( itmc->distToClosestMCPV < m_dzIsolated ) nRecMCPV_close++;
    }
  }

  // rblemcpv is already sorted

  // highest mult
  if ( rblemcpv.size() > 0 ) {
    nMCPV_1mult++;
    double dist = rblemcpv[0].distToClosestMCPV;
    if ( dist > m_dzIsolated ) nMCPV_isol_1mult++;
    if ( dist < m_dzIsolated ) nMCPV_close_1mult++;
    if ( rblemcpv[0].indexRecPVInfo > -1 ) {
      nRecMCPV_1mult++;
      if ( dist > m_dzIsolated ) nRecMCPV_isol_1mult++;
      if ( dist < m_dzIsolated ) nRecMCPV_close_1mult++;
    } else {
      nRecMCPV_wrong_1mult++;
    }
  }

  // second highest
  if ( rblemcpv.size() > 1 ) {
    nMCPV_2mult++;
    double dist = rblemcpv[1].distToClosestMCPV;
    if ( dist > m_dzIsolated ) nMCPV_isol_2mult++;
    if ( dist < m_dzIsolated ) nMCPV_close_2mult++;
    if ( rblemcpv[1].indexRecPVInfo > -1 ) {
      nRecMCPV_2mult++;
      if ( dist > m_dzIsolated ) nRecMCPV_isol_2mult++;
      if ( dist < m_dzIsolated ) nRecMCPV_close_2mult++;
    } else {
      nRecMCPV_wrong_2mult++;
    }
  }
  // third highest
  if ( rblemcpv.size() > 2 ) {
    nMCPV_3mult++;
    double dist = rblemcpv[2].distToClosestMCPV;
    if ( dist > m_dzIsolated ) nMCPV_isol_3mult++;
    if ( dist < m_dzIsolated ) nMCPV_close_3mult++;
    if ( rblemcpv[2].indexRecPVInfo > -1 ) {
      nRecMCPV_3mult++;
      if ( dist > m_dzIsolated ) nRecMCPV_isol_3mult++;
      if ( dist < m_dzIsolated ) nRecMCPV_close_3mult++;
    } else {
      nRecMCPV_wrong_3mult++;
    }
  }

  nMCPV_isol  = nMCPV_isol - nmrc_isol;
  nMCPV_close = nMCPV_close - nmrc_close;

  m_nMCPV += nMCPV;
  m_nRecMCPV += nRecMCPV;
  m_nMCPV_isol += nMCPV_isol;
  m_nRecMCPV_isol += nRecMCPV_isol;
  m_nMCPV_close += nMCPV_close;
  m_nRecMCPV_close += nRecMCPV_close;
  m_nFalsePV += nFalsePV;
  m_nFalsePV_real += nFalsePV_real;
  m_nMCPV_1mult += nMCPV_1mult;
  m_nRecMCPV_1mult += nRecMCPV_1mult;
  m_nMCPV_isol_1mult += nMCPV_isol_1mult;
  m_nRecMCPV_isol_1mult += nRecMCPV_isol_1mult;
  m_nMCPV_close_1mult += nMCPV_close_1mult;
  m_nRecMCPV_close_1mult += nRecMCPV_close_1mult;
  m_nRecMCPV_wrong_1mult += nRecMCPV_wrong_1mult;
  m_nMCPV_2mult += nMCPV_2mult;
  m_nRecMCPV_2mult += nRecMCPV_2mult;
  m_nMCPV_isol_2mult += nMCPV_isol_2mult;
  m_nRecMCPV_isol_2mult += nRecMCPV_isol_2mult;
  m_nMCPV_close_2mult += nMCPV_close_2mult;
  m_nRecMCPV_wrong_2mult += nRecMCPV_wrong_2mult;
  m_nRecMCPV_close_2mult += nRecMCPV_close_2mult;
  m_nMCPV_3mult += nMCPV_3mult;
  m_nRecMCPV_3mult += nRecMCPV_3mult;
  m_nMCPV_isol_3mult += nMCPV_isol_3mult;
  m_nRecMCPV_isol_3mult += nRecMCPV_isol_3mult;
  m_nMCPV_close_3mult += nMCPV_close_3mult;
  m_nRecMCPV_close_3mult += nRecMCPV_close_3mult;
  m_nRecMCPV_wrong_3mult += nRecMCPV_wrong_3mult;

  int high = 0;

  for ( itmc = rblemcpv.begin(); rblemcpv.end() != itmc; itmc++ ) {
    double x                 = -99999.;
    double y                 = -99999.;
    double dx                = -99999.;
    double dy                = -99999.;
    double dz                = -99999.;
    double r                 = -99999.;
    double zMC               = -99999.;
    double yMC               = -99999.;
    double xMC               = -99999.;
    double rMC               = -99999.;
    double z                 = -99999.;
    double errx              = -99999.;
    double erry              = -99999.;
    double errz              = -99999.;
    double errr              = -99999.;
    double minRDTrack        = 99999.;
    double maxRDTrack        = -99999.;
    double chi2              = -999999.;
    double nDoF              = -999999.;
    int    indRec            = itmc->indexRecPVInfo;
    int    reconstructed     = 0;
    int    ntracks_pvrec     = 0;
    int    nvelotracks_pvrec = 0;
    int    ntracks_pvmc      = 0;
    int    dtrcks            = 0;
    int    pvevt             = 0;
    int    mother            = 0;
    int    assoctrks         = 0;
    int    nassoctrks        = 0;

    zMC = itmc->pMCPV->position().z();
    yMC = itmc->pMCPV->position().y();
    xMC = itmc->pMCPV->position().x();
    rMC = std::sqrt( ( xMC - m_beamSpotX ) * ( xMC - m_beamSpotX ) + ( yMC - m_beamSpotY ) * ( yMC - m_beamSpotY ) );

    if ( indRec > -1 ) {
      high++;
      pvevt++;
      reconstructed = 1;
      dx            = recpvvec[indRec].position.x() - itmc->pMCPV->position().x();
      dy            = recpvvec[indRec].position.y() - itmc->pMCPV->position().y();
      dz            = recpvvec[indRec].position.z() - itmc->pMCPV->position().z();
      x             = recpvvec[indRec].position.x();
      y             = recpvvec[indRec].position.y();
      z             = recpvvec[indRec].position.z();
      // zMC = itmc->pMCPV->position().z();
      r    = std::sqrt( ( x - m_beamSpotX ) * ( x - m_beamSpotX ) + ( y - m_beamSpotY ) * ( y - m_beamSpotY ) );
      errx = recpvvec[indRec].positionSigma.x();
      erry = recpvvec[indRec].positionSigma.y();
      errz = recpvvec[indRec].positionSigma.z();
      errr = std::sqrt( ( ( x * errx ) * ( x * errx ) + ( y * erry ) * ( y * erry ) ) / ( x * x + y * y ) );
      ntracks_pvrec     = recpvvec[indRec].nTracks;
      nvelotracks_pvrec = recpvvec[indRec].nVeloTracks;
      ntracks_pvmc      = itmc->pMCPV->products().size();
      dtrcks            = ntracks_pvmc - ntracks_pvrec;
      minRDTrack        = recpvvec[indRec].minTrackRD;
      maxRDTrack        = recpvvec[indRec].maxTrackRD;
      mother            = recpvvec[indRec].mother;
      chi2              = recpvvec[indRec].chi2;
      nDoF              = recpvvec[indRec].nDoF;

      // Filling histograms
      if ( m_produceHistogram ) {
        plot( itmc->pMCPV->position().x(), 1001, "xmc", -0.25, 0.25, 50 );
        plot( itmc->pMCPV->position().y(), 1002, "ymc", -0.25, 0.25, 50 );
        plot( itmc->pMCPV->position().z(), 1003, "zmc", -20, 20, 50 );
        plot( recpvvec[indRec].position.x(), 1011, "xrd", -0.25, 0.25, 50 );
        plot( recpvvec[indRec].position.y(), 1012, "yrd", -0.25, 0.25, 50 );
        plot( recpvvec[indRec].position.z(), 1013, "zrd", -20, 20, 50 );
        plot( dx, 1021, "dx", -0.25, 0.25, 50 );
        plot( dy, 1022, "dy", -0.25, 0.25, 50 );
        plot( dz, 1023, "dz", -0.5, 0.5, 50 );
        plot( dx / errx, 1031, "pullx", -5., 5., 50 );
        plot( dy / erry, 1032, "pully", -5., 5., 50 );
        plot( dz / errz, 1033, "pullz", -5., 5., 50 );
        plot( double( ntracks_pvrec ), 1041, "ntracks_pvrec", 0., 150., 50 );
        plot( double( dtrcks ), 1042, "mcrdtracks", 0., 150., 50 );
        plot( double( nvelotracks_pvrec ), 1043, "nvelotracks_pvrec", 0., 150., 50 );
        if ( pvevt == 1 ) {
          plot( double( recpvvec.size() ), 1051, "nPVperEvt", -0.5, 5.5, 6 );
          for ( int ipvrec = 0; ipvrec < (int)recpvvec.size(); ipvrec++ ) {
            assoctrks = assoctrks + recpvvec[ipvrec].nTracks;
          }
          nassoctrks = vecOfTracks.size() - assoctrks;
          plot( double( nassoctrks ), 1052, "nassoctrks", 0., 150., 50 );
        }
      }
    }

    int    isolated     = 0;
    double dist_closest = itmc->distToClosestMCPV;
    if ( dist_closest > m_dzIsolated ) { isolated = 1; }

    // Filling ntuple
    if ( m_produceNtuple ) {
      Tuple myTuple = nTuple( 101, "PV_nTuple", CLID_ColumnWiseTuple );
      myTuple->column( "reco", double( reconstructed ) );
      myTuple->column( "isol", double( isolated ) );
      myTuple->column( "ntracks", double( ntracks_pvrec ) );
      myTuple->column( "nvelotracks", double( nvelotracks_pvrec ) );
      myTuple->column( "nrectrmc", double( itmc->nRecTracks ) );
      myTuple->column( "dzclose", dist_closest );
      myTuple->column( "nmcpv", double( rblemcpv.size() ) );
      myTuple->column( "nmcallpv", double( mcpvvec.size() ) );
      myTuple->column( "nrecpv", double( recpvvec.size() ) );
      myTuple->column( "decayCharm", double( itmc->decayCharm ) );
      myTuple->column( "decayBeauty", double( itmc->decayBeauty ) );
      myTuple->column( "multi", double( high ) );
      myTuple->column( "dx", dx );
      myTuple->column( "dy", dy );
      myTuple->column( "dz", dz );
      myTuple->column( "x", x );
      myTuple->column( "y", y );
      myTuple->column( "r", r );
      myTuple->column( "zMC", zMC );
      myTuple->column( "yMC", yMC );
      myTuple->column( "xMC", xMC );
      myTuple->column( "rMC", rMC );
      myTuple->column( "z", z );
      myTuple->column( "xBeam", m_beamSpotX );
      myTuple->column( "yBeam", m_beamSpotY );
      myTuple->column( "errx", errx );
      myTuple->column( "erry", erry );
      myTuple->column( "errz", errz );
      myTuple->column( "errr", errr );
      myTuple->column( "minRDTrack", minRDTrack );
      myTuple->column( "maxRDTrack", maxRDTrack );
      myTuple->column( "mother", double( mother ) );
      myTuple->column( "evtnr", double( m_nevt ) );
      myTuple->column( "chi2", double( chi2 ) );
      myTuple->column( "nDoF", double( nDoF ) );
      myTuple->column( "size_tracks", double( sTr ) );
      myTuple->column( "size_mcp", double( sMCP ) );
      myTuple->column( "mcpvrec", double( nmrc ) );
      myTuple->write();
    }
  }

  return;
}

void PrimaryVertexChecker::match_mc_vertex_by_tracks( int ipv, std::vector<RecPVInfo>& rinfo,
                                                      std::vector<MCPVInfo>& mcpvvec ) const {

  LHCb::RecVertex*                          invtx = rinfo[ipv].pRECPV;
  typedef const SmartRefVector<LHCb::Track> PVTRACKS;
  PVTRACKS&                                 tracksIn = invtx->tracks();
  PVTRACKS::const_iterator                  itin;

  int    indexmc  = -1;
  double ratiomax = 0.;
  for ( int imc = 0; imc < (int)mcpvvec.size(); imc++ ) {
    if ( mcpvvec[imc].indexRecPVInfo > -1 ) continue;
    int ntrin = 0;
    for ( std::vector<LHCb::Track*>::iterator itr = mcpvvec[imc].m_recTracksInMCPV.begin();
          itr != mcpvvec[imc].m_recTracksInMCPV.end(); itr++ ) {
      for ( itin = tracksIn.begin(); itin != tracksIn.end(); itin++ ) {
        const LHCb::Track* ptr = *itin;
        if ( ptr == *itr ) {
          ntrin++;
          break;
        }
      }
    }
    double ratio = 1. * ntrin / tracksIn.size();
    if ( ratio > ratiomax ) {
      ratiomax = ratio;
      indexmc  = imc;
    }
  } // imc
  if ( ratiomax > 0.05 ) {
    rinfo[ipv].indexMCPVInfo        = indexmc;
    mcpvvec[indexmc].indexRecPVInfo = ipv;
  }
}

void PrimaryVertexChecker::match_mc_vertex_by_distance( int ipv, std::vector<RecPVInfo>& rinfo,
                                                        std::vector<MCPVInfo>& mcpvvec ) const {

  double mindist = 999999.;
  int    indexmc = -1;

  for ( int imc = 0; imc < (int)mcpvvec.size(); imc++ ) {
    if ( mcpvvec[imc].indexRecPVInfo > -1 ) continue;
    double dist = fabs( mcpvvec[imc].pMCPV->position().z() - rinfo[ipv].position.z() );
    if ( dist < mindist ) {
      mindist = dist;
      indexmc = imc;
    }
  }
  if ( indexmc > -1 ) {
    if ( mindist < 5.0 * rinfo[ipv].positionSigma.z() ) {
      rinfo[ipv].indexMCPVInfo        = indexmc;
      mcpvvec[indexmc].indexRecPVInfo = ipv;
    }
  }
}

std::vector<MCPVInfo>::iterator PrimaryVertexChecker::closestMCPV( std::vector<MCPVInfo>&           rblemcpv,
                                                                   std::vector<MCPVInfo>::iterator& itmc ) const {

  std::vector<MCPVInfo>::iterator itret   = rblemcpv.end();
  double                          mindist = 999999.;
  if ( rblemcpv.size() < 2 ) return itret;
  std::vector<MCPVInfo>::iterator it;
  for ( it = rblemcpv.begin(); it != rblemcpv.end(); it++ ) {
    if ( it->pMCPV != itmc->pMCPV ) {
      double dist = ( it->pMCPV->position() - itmc->pMCPV->position() ).R();
      if ( dist < mindist ) {
        mindist = dist;
        itret   = it;
      }
    }
  }
  return itret;
}

void PrimaryVertexChecker::collectProductss( LHCb::MCVertex* mcpv, LHCb::MCVertex* mcvtx,
                                             std::vector<LHCb::MCParticle*>& allprods ) const {

  SmartRefVector<LHCb::MCParticle>           daughters = mcvtx->products();
  SmartRefVector<LHCb::MCParticle>::iterator idau;
  for ( idau = daughters.begin(); idau != daughters.end(); idau++ ) {
    double dv2 = ( mcpv->position() - ( *idau )->originVertex()->position() ).Mag2();
    if ( dv2 > ( 100. * Gaudi::Units::mm ) * ( 100. * Gaudi::Units::mm ) ) continue;
    LHCb::MCParticle* pmcp = *idau;
    allprods.push_back( pmcp );
    SmartRefVector<LHCb::MCVertex>           decays = ( *idau )->endVertices();
    SmartRefVector<LHCb::MCVertex>::iterator ivtx;
    for ( ivtx = decays.begin(); ivtx != decays.end(); ivtx++ ) { collectProductss( mcpv, *ivtx, allprods ); }
  }
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrimaryVertexChecker::finalize() {

  debug() << "==> Finalize" << endmsg;

  info() << " ============================================" << endmsg;
  info() << " Efficiencies for reconstructible MC vertices: " << endmsg;
  info() << " ============================================" << endmsg;
  info() << " " << endmsg;

  info() << " MC PV is reconstructible if at least " << m_nTracksToBeRecble << "  tracks are reconstructed" << endmsg;
  info() << " MC PV is isolated if dz to closest reconstructible MC PV >  " << m_dzIsolated << " mm" << endmsg;
  std::string ff = "by counting tracks";
  if ( !m_matchByTracks ) ff = "by dz distance";
  info() << " REC and MC vertices matched:  " << ff << endmsg;

  info() << " " << endmsg;

  printRat( "All", m_nRecMCPV, m_nMCPV );
  printRat( "Isolated", m_nRecMCPV_isol, m_nMCPV_isol );
  printRat( "Close", m_nRecMCPV_close, m_nMCPV_close );
  printRat( "False rate", m_nFalsePV, m_nRecMCPV + m_nFalsePV );

  if ( debugLevel() ) { printRat( "Real false rate", m_nFalsePV_real, m_nRecMCPV + m_nFalsePV_real ); }

  info() << endmsg;
  printRat( "L0 accepted PV of B", m_nRecL0PvOfB, m_nL0PvOfB );
  printRat( "False PV as B", m_nRecBFalse, m_nBFalse );
  info() << endmsg;

  info() << "      --------------------------------------------" << endmsg;
  info() << "           Substatistics: " << endmsg;
  info() << "      --------------------------------------------" << endmsg;
  info() << "      1st PV (highest multiplicity): " << endmsg;
  printRat( "All", m_nRecMCPV_1mult, m_nMCPV_1mult );
  printRat( "Isolated", m_nRecMCPV_isol_1mult, m_nMCPV_isol_1mult );
  printRat( "Close", m_nRecMCPV_close_1mult, m_nMCPV_close_1mult );

  info() << "      ---------------------------------------" << endmsg;
  info() << "      2nd PV: " << endmsg;
  printRat( "All", m_nRecMCPV_2mult, m_nMCPV_2mult );
  printRat( "Isolated", m_nRecMCPV_isol_2mult, m_nMCPV_isol_2mult );
  printRat( "Close", m_nRecMCPV_close_2mult, m_nMCPV_close_2mult );

  info() << "      ---------------------------------------" << endmsg;
  info() << "      3rd PV: " << endmsg;
  printRat( "All", m_nRecMCPV_3mult, m_nMCPV_3mult );
  printRat( "Isolated", m_nRecMCPV_isol_3mult, m_nMCPV_isol_3mult );
  printRat( "Close", m_nRecMCPV_close_3mult, m_nMCPV_close_3mult );

  info() << " " << endmsg;
  if ( debugLevel() ) {
    info() << " * Real false rate means: no visible MC PV within 5 sigma of REC PV."
           << " Visible MC PV: 2 tracks reconstructed" << endmsg;
    info() << " " << endmsg;
  }
  const AIDA::IHistogram1D* dx    = histo( HistoID( 1021 ) );
  const AIDA::IHistogram1D* pullx = histo( HistoID( 1031 ) );
  const AIDA::IHistogram1D* dy    = histo( HistoID( 1022 ) );
  const AIDA::IHistogram1D* pully = histo( HistoID( 1032 ) );
  const AIDA::IHistogram1D* dz    = histo( HistoID( 1023 ) );
  const AIDA::IHistogram1D* pullz = histo( HistoID( 1033 ) );
  if ( dx ) {
    info() << "      ---------------------------------------" << endmsg;
    info() << "dx:    "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f", dx->mean(),
                      Gaudi::Utils::HistoStats::meanErr( dx ), dx->rms(), Gaudi::Utils::HistoStats::rmsErr( dx ) )
           << endmsg;
  }
  if ( dy ) {
    info() << "dy:    "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f", dy->mean(),
                      Gaudi::Utils::HistoStats::meanErr( dy ), dy->rms(), Gaudi::Utils::HistoStats::rmsErr( dy ) )
           << endmsg;
  }
  if ( dz ) {
    info() << "dz:    "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f", dz->mean(),
                      Gaudi::Utils::HistoStats::meanErr( dz ), dz->rms(), Gaudi::Utils::HistoStats::rmsErr( dz ) )
           << endmsg;
  }
  info() << "      ---------------------------------------" << endmsg;
  if ( pullx ) {
    info() << "pullx: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f", pullx->mean(),
                      Gaudi::Utils::HistoStats::meanErr( pullx ), pullx->rms(),
                      Gaudi::Utils::HistoStats::rmsErr( pullx ) )
           << endmsg;
  }
  if ( pully ) {
    info() << "pully: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f", pully->mean(),
                      Gaudi::Utils::HistoStats::meanErr( pully ), pully->rms(),
                      Gaudi::Utils::HistoStats::rmsErr( pully ) )
           << endmsg;
  }
  if ( pullz ) {
    info() << "pullz: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f", pullz->mean(),
                      Gaudi::Utils::HistoStats::meanErr( pullz ), pullz->rms(),
                      Gaudi::Utils::HistoStats::rmsErr( pullz ) )
           << endmsg;
  }
  info() << " ============================================" << endmsg;

  return GaudiTupleAlg::finalize(); // Must be called after all other actions
}

void PrimaryVertexChecker::printRat( std::string mes, int a, int b ) {

  double rat = 0.;
  if ( b > 0 ) rat = 1.0 * a / b;

  // reformat message
  unsigned int len  = 20;
  std::string  pmes = mes;
  while ( pmes.length() < len ) { pmes += " "; }
  pmes += " : ";

  info() << pmes << format( " %6.3f ( %7d / %8d )", rat, a, b ) << endmsg;
}

void PrimaryVertexChecker::count_reconstructed_tracks( std::vector<MCPVInfo>&     mcpvvec,
                                                       std::vector<LHCb::Track*>& vecOfTracks, std::string trackLoc ) const {

  LinkedTo<LHCb::MCParticle> trackMClink( eventSvc(), msgSvc(), trackLoc );

  // Find # of reconstructed tracks of every MC PV
  std::vector<MCPVInfo>::iterator itinfomc;
  for ( itinfomc = mcpvvec.begin(); mcpvvec.end() != itinfomc; itinfomc++ ) {
    LHCb::MCVertex* avtx = itinfomc->pMCPV;

    SmartRefVector<LHCb::MCParticle> parts = avtx->products();
    std::vector<LHCb::MCParticle*>   allproducts;
    collectProductss( avtx, avtx, allproducts );

    LHCb::Track*                             recTrack = 0;
    std::vector<LHCb::MCParticle*>::iterator imcp;
    for ( imcp = allproducts.begin(); allproducts.end() != imcp; imcp++ ) {
      LHCb::MCParticle* pmcp   = *imcp;
      int               isReco = 0;
      for ( std::vector<LHCb::Track*>::iterator itrec = vecOfTracks.begin(); itrec != vecOfTracks.end(); itrec++ ) {
        LHCb::MCParticle* partmc = trackMClink.first( ( *itrec )->key() );
        if ( partmc && partmc == pmcp ) {
          recTrack = ( *itrec );
          if ( !m_requireVelo || recTrack->hasVelo() ) {
            isReco = 1;
            break;
          }
        }
      }
      if ( pmcp->particleID().threeCharge() != 0 && isReco ) {
        double dv2 = ( avtx->position() - pmcp->originVertex()->position() ).Mag2();
        if ( dv2 < 0.0001 ) {
          itinfomc->m_mcPartInMCPV.push_back( pmcp );
          itinfomc->m_recTracksInMCPV.push_back( recTrack );
        }
      }
    }
    itinfomc->nRecTracks = itinfomc->m_mcPartInMCPV.size();
  }
}

int PrimaryVertexChecker::count_velo_tracks( LHCb::RecVertex* RecVtx ) {
  SmartRefVector<LHCb::Track> vtx_tracks  = RecVtx->tracks();
  int                         nVeloTracks = 0;
  for ( unsigned int it = 0; it < vtx_tracks.size(); it++ ) {
    const LHCb::Track* ptr = vtx_tracks[it];
    if ( ptr->hasVelo() ) nVeloTracks++;
  }
  return nVeloTracks;
}

void PrimaryVertexChecker::count_reconstructible_mc_particles( std::vector<MCPVInfo>& mcpvvec ) const {

  const MCTrackInfo               trInfo = make_MCTrackInfo( eventSvc(), msgSvc() );
  std::vector<MCPVInfo>::iterator itinfomc;
  for ( itinfomc = mcpvvec.begin(); mcpvvec.end() != itinfomc; itinfomc++ ) {
    LHCb::MCVertex*                  avtx = itinfomc->pMCPV;
    std::vector<LHCb::MCParticle*>   mcPartInMCPV;
    SmartRefVector<LHCb::MCParticle> parts = avtx->products();
    std::vector<LHCb::MCParticle*>   allproducts;
    collectProductss( avtx, avtx, allproducts );
    std::vector<LHCb::MCParticle*>::iterator imcp;

    for ( imcp = allproducts.begin(); allproducts.end() != imcp; imcp++ ) {

      LHCb::MCParticle* pmcp = *imcp;

      if ( pmcp->particleID().threeCharge() != 0 && ( !m_requireVelo || trInfo.hasVelo( pmcp ) ) ) {

        if ( pmcp->particleID().hasBottom() ) { itinfomc->decayBeauty = 1.0; }
        if ( pmcp->particleID().hasCharm() ) { itinfomc->decayCharm = 1.0; }
        if ( pmcp->particleID().threeCharge() != 0 && ( !m_requireVelo || trInfo.hasVelo( pmcp ) ) ) {

          double dv2 = ( avtx->position() - pmcp->originVertex()->position() ).Mag2();
          if ( dv2 < 0.0000001 && pmcp->p() > 100. * Gaudi::Units::MeV ) { mcPartInMCPV.push_back( pmcp ); }
        }
      }
      itinfomc->nRecTracks = mcPartInMCPV.size();
    }
  }
}
int PrimaryVertexChecker::check_mother_particle( LHCb::RecVertex* pv, std::string trackLoc ) const {

  int         motherPart = 0;
  const auto& tracksIn   = pv->tracks();

  LinkedTo<LHCb::MCParticle, LHCb::Track> link( eventSvc(), msgSvc(), trackLoc );
  for ( auto itin = tracksIn.begin(); itin != tracksIn.end(); itin++ ) {
    const LHCb::Track* ptr  = *itin;
    LHCb::MCParticle*  part = nullptr;
    part                    = link.first( ptr->key() );
    if ( part != nullptr ) {
      const LHCb::MCParticle* part1 = part;
      int                     i     = 0;
      while ( part1 != nullptr ) {
        // std::cout<<"X"<<std::endl;
        const LHCb::MCParticle* mother = part1->mother();
        if ( mother != nullptr ) {
          i     = i + 1;
          part1 = mother;
          /// std::cout<<i<<": "<<part1->particleID().pid()<<std::endl;
        } else {
          break;
        }
      }

      if ( part1->particleID().hasBottom() == true ) { motherPart = 2.0; }
      if ( part1->particleID().hasCharm() == true ) { motherPart = 1.0; }
    }
  }
  return motherPart;
}

// bool PrimaryVertexChecker::getInputTracks( std::vector<LHCb::Track*>& vecOfTracks ) const {

//   // std::string tracksName = m_inputTracksName;

//   // if ( m_inputTracksName == "Offline" ) {
//   //   tracksName = LHCb::TrackLocation::Default;
//   // } else if ( m_inputTracksName == "3D" ) {
//   //   tracksName = LHCb::TrackLocation::Velo;
//   // } else if ( m_inputTracksName == "2D" ) {
//   //   tracksName = LHCb::TrackLocation::RZVelo;
//   // }

//   // if ( tracksName == "none" ) {
//   //   debug() << " Tracks not specified " << tracksName << endmsg;
//   //   return false;
//   // }

//   //LHCb::Tracks* usedTracks = getOrCreate<LHCb::Tracks, LHCb::Tracks>( LHCb::TrackLocation::Default );
//   if ( usedTracks->size() == 0 ) return false;

//   std::vector<LHCb::Track*>::const_iterator itT;
//   for ( itT = usedTracks->begin(); usedTracks->end() != itT; itT++ ) {
//     LHCb::Track* ptr = ( *itT );
//     vecOfTracks.push_back( ptr );
//   }

//   return true;
// }

// bool PrimaryVertexChecker::getInputVertices( std::vector<LHCb::RecVertex*>& vecOfVertices ) const {

//   // std::string verticesName = m_inputVerticesName;

//   // if ( m_inputVerticesName == "Offline" ) {
//   //   verticesName = LHCb::RecVertexLocation::Primary;
//   // } else if ( m_inputVerticesName == "3D" ) {
//   //   verticesName = LHCb::RecVertexLocation::Velo3D;
//   // } else if ( m_inputVerticesName == "2D" ) {
//   //   verticesName = LHCb::RecVertexLocation::Velo2D;
//   // }

//   // if ( verticesName == "none" ) {
//   //   debug() << " Vertices not specified " << verticesName << endmsg;
//   //   return false;
//   // }

//   //LHCb::RecVertices* recoVertices = getOrCreate<LHCb::RecVertices, LHCb::RecVertices>( LHCb::RecVertexLocation::Primary );

//   std::vector<LHCb::RecVertex*>::const_iterator itVer;
//   for ( itVer = recoVertices->begin(); recoVertices->end() != itVer; itVer++ ) {
//     LHCb::RecVertex* ppv = ( *itVer );
//     vecOfVertices.push_back( ppv );
//   }

//   return true;
// }
